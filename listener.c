#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#define SOCK_PATH "/home/rfollensbee/file"

//Most of the following found at http://beej.us/guide/bgnet/output/html/multipage/clientserver.html#simpleserver
int main(void){

	int s, n, len, done;
    struct sockaddr_un local;
	char buffer[256];

	//Clearing the buffer
	for (int a = 0; a < 256; a++){
		buffer[a] = '\0';
	}

	//Error handeling
	if ((s = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(1);
	}

	printf("Trying to connect...\n");

    local.sun_family = AF_UNIX;

	strcpy(local.sun_path, SOCK_PATH);

	len = strlen(local.sun_path) + sizeof(local.sun_family);

	//Error handeling
    if (connect(s, (struct sockaddr *)&local, len) == -1) {
    	perror("connect");
    	exit(1);
	}

	printf("Connected on socket %d\n", s);

	done = 0;

	//Loop that will run at least once, it captures what is sent by dispatcher, performs error checking,
	//then prints out everything from the buffer before clearing it
	do {
		n = recv(s, buffer, sizeof(buffer), 0);

		if (strlen(buffer) <= 0){
			printf("Exiting...\n");
			close(s);
			exit(1);
		}

		if (n <= 0){
			if (n < 0){
				perror("recv");
			}
			done = 1;
		}

		printf("From dispatcher: %s\n", buffer);		

		//Clearing the buffer
		for (int a = 0; a < 256; a++){
			buffer[a] = '\0';
		}


	} while (!done);

	close(s);

	return 0;

}
