
CFLAGS+=-std=c11 
CFLAGS+=-pthread -Wall -Wextra -Wpedantic
CFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline
CFLAGS+=-D _POSIX_C_SOURCE>=1

all: dispatcher listener

dispatcher:

listener:

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: all

.PHONY: clean debug profile

clean:
	-rm dispatcher listener *.o

debug: CFLAGS+=-g
debug: all
