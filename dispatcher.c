#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/time.h>
#include <sys/stat.h>

#define SOCK_PATH "/home/rfollensbee/file"

//Most of the following found at http://beej.us/guide/bgnet/output/html/multipage/clientserver.html#simpleserver
// and at http://beej.us/guide/bgipc/output/print/bgipc_A4.pdf
int main(void){

	//Declare variables to be used
	int s, fdmax, newfd, nbytes, len;  
    struct sockaddr_un local;
	struct sockaddr_storage remoteaddr;
	fd_set master;		
	fd_set read_fds;		
	char buffer[256];
	socklen_t addrlen;
	FD_ZERO (&master);		
	FD_ZERO (&read_fds);

	//Clear the buffer
	for (int a = 0; a < 256; a++){
		buffer[a] = '\0';
	}

	//Error handeling 
	if ((s = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }
	
	//Found at http://stackoverflow.com/questions/24194961/how-do-i-use-setsockoptso-reuseaddr
	int enable = 1;
	if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0){
    	perror("setsockopt(SO_REUSEADDR) failed");
		close(s);
	}

	printf("Trying to connect...\n");

    local.sun_family = AF_UNIX;

	strcpy(local.sun_path, SOCK_PATH);

    unlink(local.sun_path);

	len = strlen(local.sun_path) + sizeof(local.sun_family);

	//Error handeling
    if (bind(s, (struct sockaddr *)&local, len) == -1){
		perror("bind");
		exit(1);
	}

	//Make file accessable
	chmod(SOCK_PATH, 0777);

	//Limit the amount of listeners
	if (listen(s, 5) == -1) {
		perror("listen");
		exit(1);
	}

	//Add the listener to the master set
	fdmax = s;
	FD_SET(s, &master);
	FD_SET(STDIN_FILENO, &master);

	//Main loop that runs through all existing connections looking for data to read
	while(1) {
		read_fds = master;		
		if (select(fdmax + 1, &read_fds, NULL, NULL, NULL) == -1) {
			perror ("select");
			exit(1);
		}	

		int i;
		for (i = 0; i <= fdmax; i++) {
			if (FD_ISSET (i, &read_fds)) {	
				//For new connections
				if (i == s) {
					addrlen = sizeof(remoteaddr);
					newfd = accept(s, (struct sockaddr *) &remoteaddr, &addrlen);

					if (newfd == -1) {
						perror("accept");
					} 
					else {
						FD_SET(newfd, &master);	
						if (newfd > fdmax) {	
			  				fdmax = newfd;
						}
					}
				}
				//For current connections
				else if (i == STDIN_FILENO) {
					//Will read from standard input
					if ((nbytes = read(i, buffer, sizeof(buffer))) > 0) {
						//The EOT check for quit
						if (strncmp(buffer, "quit\n", 5) == 0){
							printf("Exiting...\n");
							exit(1);
						}
						buffer[nbytes] = '\0';
						printf("Read from stdin :%s\n", buffer);
						//Sends to each connection
						for (i = 0; i <= fdmax; i++) {  
							if (FD_ISSET (i, &master) && i != STDIN_FILENO && i != s) {					
								if (send(i, buffer, nbytes, 0) == -1) {
			  						perror ("send");
								}
							}
		  				}
					}
				}
			}
		}
	}

	return 0;

}
